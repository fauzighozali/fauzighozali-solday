/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
// import {Provider} from "redux"
// import configureStore from "./App/Store/ConfigureStore"

// const store = configureStore()
// const RNRedux = () =>(
//     <Provider>
//         <App/>
//     </Provider>
// )

// AppRegistry.registerComponent(appName, () => RNRedux);
AppRegistry.registerComponent(appName, () => App);
