import React, { Component } from 'react';
import {
  StyleSheet,
  FlatList,
  ActivityIndicator,
  View,
  AsyncStorage,
  Alert,
  Image,
  Dimensions
} from 'react-native'

import { urlDev } from '../../lib/server'
import { ListItem } from '../../Components/ListItem'
import { DialogInput, DialogMessage2 } from '../../Components/DialogInput'
import { ButtonOrange } from '../../Components/PlaceButton'

const width_max = Dimensions.get('window').width
const height_max = Dimensions.get('window').height

export default class SpeakersScreen extends Component {

  state = {
    isLoading: true,
    isShowDialog: false,
    isDialogVisible: false,
    statusSpeacker: false,
    message: '',
    selectSpeakerId: null,
    track: '',
  }

  constructor() {
    super()
    AsyncStorage.getItem('token', (error, result) => {
      if (result) {
        this.setState({
          token: result
        });

        this.getSpeaker(result)

        const { navigation } = this.props;
        const track = navigation.getParam('track');
        this.setState({ track: track })

      }
    })
  }

  getSpeaker = (token) => {
    const { navigation } = this.props;
    const track = navigation.getParam('track');
    this.setState({ track: track })

    return fetch(urlDev + '/api/v1/topic?filter=[{"property":"event_topics.track","value":"' + this.state.track + '","operator":"="}]',
      {
        method: "GET",
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        },
      })
      .then((resp) => {
        // console.warn(resp)
        this.setState({
          isLoading: false,
        })
        if (resp.status === 200) {
          return resp.json().then((json) => {
            // console.warn(json)
            if (json.success === true) {
              if (json.total === 0) {
                this.setState({
                  statusSpeacker: true
                })
              } else {
                this.setState({
                  statusSpeacker: false,
                  dataSource: json.rows,
                }, function () {

                });
              }
            }
          })
        }
        else if (resp.status === 401) {
          // Alert.alert('Message ', 'Access is denied')
          this.setState({isLoading:true})
            this.getSpeaker()

        } else if (resp.status === 503) {
          Alert.alert('Message ', 'Service Unavailable')
        }
      })

      .catch((error) => {
        console.error(error);
      });
  }


  selectSpeaker = id => {
    // console.warn(id)
    this.props.navigation.navigate('Session', {
      'speaker': this.state.dataSource[id],
      'track': this.state.track
    });
  }

  updateCode = (code) => {
    this.setState({
      code: code
    })
  }


  showDialog = () => {
    this.setState({
      isDialogVisible: true
    })
  }

  hendleSubmit = () => {
    console.warn(this.state.track + ' ' + this.state.code)
    // sincronize data
    this.setState({
      isDialogVisible: false,
      isLoading: true
    })
    if (this.state.code !== '') {
      return fetch(urlDev + '/api/v1/topic', {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.state.token
        },
        body: JSON.stringify({
          "event_topic_code": this.state.code,
          "track": this.state.track,
        }),
      })
        .then((resp) => {
          console.warn(resp)
          this.getSpeaker(this.state.token)

          return resp.json().then((json) => {
            console.warn('postcode : ' + json)
            this.setState({
              isLoading: false,
            })

            if (json.success == true) {
              this.setState({
                isShowDialog: true,
                responseData: json.rows,
                statusSpeacker: false,
                message: json.message
              }, function () {

              })
            } else {
              this.setState({
                isShowDialog: true,
                message: json.message
              })
            }
          })

        })
        .catch((error) => {
          console.error(error);
        });
    } else {
      alert('please enter the track code')
    }

  }

  hendleCancel = () => {
    this.setState({
      isDialogVisible: false
    })
  }

  render() {

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }
    if (this.state.statusSpeacker) {
      // console.warn('kosong')
      return (
        <View style={{ flex: 1 }}>
          <Image
            animationType="slide"
            style={{ height: height_max, width: width_max, position: 'absolute' }}
            source={require('../../Images/speacker_null.jpg')}>
          </Image>
          <View style={[{ flex: 1, justifyContent: 'flex-end' }, styles.postCode]}>
            <ButtonOrange title={'Input Code'} onPress={this.showDialog} />
          </View>
          <DialogInput
            visible={this.state.isDialogVisible}
            placeHolder={"code"}
            updateCode={this.updateCode}
            hendleCancel={this.hendleCancel}
            hendleSubmit={this.hendleSubmit}
          />
          <DialogMessage2
            visible={this.state.isShowDialog}
            description={
              this.state.message
            }
            submit={() => {
              this.setState({ isShowDialog: false })
            }}
          />
        </View>
      )
    }

    return (
      <View style={styles.container}>

        <FlatList
          data={this.state.dataSource}
          renderItem={({ item }) =>
            <ListItem
              id={this.state.dataSource.indexOf(item)}
              image={{ uri: item.profile_pic }}
              onShowDialog={this.selectSpeaker}
              name={item.name}
              title={item.title}
              corporate={item.corporate}
            />
          } keyExtractor={({ id }, index) => id}
        />
        <View style={styles.postCode}>
          <ButtonOrange title={'Input Code'} onPress={this.showDialog} />
        </View>
        <DialogInput
          visible={this.state.isDialogVisible}
          placeHolder={"code"}
          updateCode={this.updateCode}
          hendleCancel={this.hendleCancel}
          hendleSubmit={this.hendleSubmit}
        />
        <DialogMessage2
          visible={this.state.isShowDialog}
          description={
            this.state.message
          }
          submit={() => {
            this.setState({ isShowDialog: false })
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 10
  },
  listItem: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderWidth: 0.5,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0.5,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    marginBottom: 5
  },
  item: {
    marginLeft: 10,
    alignItems: 'flex-start'
  },
  image: {
    width: 40,
    height: 40,
    borderRadius: 50,
    shadowColor: '#add'
  },
  postCode: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 8,
  }
})
