import React, { Component } from 'react';
import {
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Text,
  View,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Image,
  Dimensions,
  ImageBackground
} from 'react-native'

import { urlDev } from '../../lib/server'
import { DialogInput } from '../../Components/DialogInput'
import { ButtonOrange } from '../../Components/PlaceButton'


const width_max = Dimensions.get('window').width
const height_max = Dimensions.get('window').height


class TrackScreen extends Component {


  constructor() {
    super()
    this.state = {
      token: '',
      isLoading: false,
      responseData: [],
      statusTrack: false,
      totalTrack: 0,
    }
  }

  // componentDidMount(){
  //   console.warn(this.state.token)
  // }
  componentDidMount() {
    AsyncStorage.getItem('token', (error, result) => {
      if (result) {
        this.setState({
          token: result
        });
        console.warn('result')
        this.getDataTrack(result)
      }
    })
  }

  getDataTrack = (token) => {
    this.setState({
      isLoading: true
    })
    return fetch(urlDev + '/api/v1/track', {
      method: "GET",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      },
    })
      .then((resp) => {
        console.warn(resp)
        this.setState({
          isLoading: false
        })

        if (resp.status === 200) {
          return resp.json().then((json) => {
            console.warn(json)

            if (json.success == true) {
              // console.warn(json.total)
              this.setState({
                totalTrack: json.total
              })
              if (json.total === 0) {
                this.setState({
                  statusTrack: true
                })
              } else {
                console.warn(json.rows)
                this.setState({
                  statusTrack: false,
                  responseData: json.rows
                }, function () {

                })
              }

            } else {
              Alert.alert('Message ', json.message)
            }
          })
        }
        else if (resp.status === 401) {
          this.setState({ isLoading: true })
          this.getDataTrack()

        } else if (resp.status === 503) {
          Alert.alert('Message ', 'Service Unavailable')
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }
    if (this.state.statusTrack) {
      // console.warn('kosong')
      return (
        <View style={{ flex: 1, }}>
          <Image
            animationType="slide"
            style={{ height: height_max, width: width_max }}
            source={require('../../Images/track_null.jpg')}>
          </Image>
          <TouchableOpacity onPress={
            () => this.getDataTrack(this.state.token)
          }
            style={styles.floatButton}
          >
            <Image
              source={require('../../Images/reload.png')}
              style={styles.imageFloat}
            />
          </TouchableOpacity>

        </View>
      )
    }

    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.responseData}
          renderItem={({ item }) =>
            <TouchableOpacity onPress={() => {
              this.props.navigation.navigate('Speaker', { track: item.track });
            }}>
              <View style={styles.listItem}>
                <Text style={{ fontSize: 16 }}>
                  {item.title}
                </Text>
              </View>
            </TouchableOpacity>
          } keyExtractor={({ id }, index) => id}
        />
        <TouchableOpacity onPress={() => {
          console.warn('tes')
          this.getDataTrack(this.state.token)
        }}
          style={styles.floatButton}>
          <Image
            source={require('../../Images/reload.png')}
            style={styles.imageFloat}
          />
        </TouchableOpacity>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  listItem: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    marginLeft: 16,
    marginRight: 16,
    marginTop: 5
  },
  item: {
    marginLeft: 10,
    alignItems: 'flex-start'
  },
  floatButton: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
  },
  imageFloat: {
    height: 60,
    width: 60,
  }
})

export default TrackScreen
