import React, {Component} from 'react';
import { ActivityIndicator, AsyncStorage,Linking, Alert, View, Text, StyleSheet, ScrollView, TouchableOpacity,Swiper} from 'react-native';
import { AppRegistry} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Button, TextInput } from 'react-native';
import { WebView } from 'react-native-gesture-handler';
import  RadioForm  from 'react-native-radio-form';
import LoginScreen from '../Login';
import { urlDev } from '../../lib/server'
import { CheckBox } from 'react-native-elements';

const mockData = [
    {
        label: 'label1',
        value: 'fi'
    },
    {
        label: 'label2',
        value: 'se'
    },
    {
        label: 'label3',
        value: 'th'
    }
]

export default class FeedbackActivity extends Component {
    static navigationOptions = 
    {
        title: 'Give Your Feedback',
    };

    // constructor()
    // {
    //     super();
    //     //console.log("asda");
    //     //const { navigation } = this.props;
      
    //         this.state = {
    //             questions: [],
    //             answers: [],
    //             idtrack: "123",
    //             id: '',
    //             isLoading: true,
    //             type: "123"
    //         }

  
    //     this.getFeedbacksFromApi();
    // }

    constructor(props) {
        super(props);

        const { navigation } = this.props;

       
        const speaker=navigation.getParam('speaker');
        
        // const asda = navigation.getParam('typeFeedback');
        console.log(this.props.typeFeedback);
        if(speaker!=null)
        {
            this.state = {
                questions: [],
                answers: [],
                answersCL: [],
                idtrack: speaker.id,
                id: '',
                isLoading: true,
                type: "",
                anwered: false
            }
        }
        else
        {
            this.state = {
                questions: [],
                answers: [],
                answersCL: [],
                idtrack: "",
                id: '',
                isLoading: true,
                type: "",
                anwered: false
            }
        }

        
     
        
      
        // console.log(speaker);

        this.getFeedbacksFromApi();
    };

    componentDidMount() {
        //BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentDidUpdate(prevProps, prevState) {
        // only update chart if the data has changed
        const { navigation } = this.props;
        const speaker=navigation.getParam('speaker');
         
        if(speaker !=null)
        {
             if (prevState.idtrack != speaker.id) {
                console.log('ganti 1');
                console.log(speaker.id);
                this.setState({idtrack: speaker.id});
                this.getFeedbacksFromApi();
            }
        }
        else
        {
            if(prevProps.typeFeedback != this.props.typeFeedback)
            {
                console.log('ganti 2');
                this.setState({idtrack: ''});
                this.getFeedbacksFromApi();
            }
        }
           
        
    
        
        //
        
    
    }

   
    
    getFeedbacksFromApi()
    {
        AsyncStorage.getItem('token', (error, result) => {
            if (result) {
                this.setState({
                    id: result
                });

        var type = "session";
        if(this.state.idtrack == '')
        {
            type = "umum";
        }

        console.log(type);
        console.log(urlDev + '/api/v1/masterfeedback?filter=[{"property":"event_feedbacks.category","value":"'+type +'","operator":"like"}]');
        return fetch(urlDev + '/api/v1/masterfeedback?filter=[{"property":"event_feedbacks.category","value":"'+type +'","operator":"like"}]', {
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this.state.id
                    },
                })
        .then((resp) => {
            // console.warn(resp)
            this.setState({
                isLoading: false
            })

            if (resp.status === 200) {
                return resp.json()
            }
            else if (resp.status === 401) {
                Alert.alert('Message ', 'Access is denied')
            } else if (resp.status === 503) {
                Alert.alert('Message ', 'Service Unavailable')
            }
        })
        .then((responseJson) => {
        
          var data = [];
          var dataCL = [];
          var i = 0;

          responseJson.rows.map ( question => { 

            if(question.type_answer == 'cl')
            {
                var j = 1;
                question.answer.map(answer => {
                    var type = 1;
                    
                    if(answer.option == '')
                    {
                        type = 2;
                    }

                    dataCL.push({"event_topic_id":this.state.idtrack, "event_feedbacks_id": question.id, "answer": answer.option, "checked": false, "type" : type});
                    j++;
                });
            }
            else
            {
                data.push({"event_topic_id":this.state.idtrack, "event_feedbacks_id": question.id, "answer": "" });
                i++;
            }
           
            
            this.setState({ answers: data });
            this.setState({ answersCL: dataCL});
            this.setState({ questions: responseJson.rows })

            
          });

          //console.log(this.state.questions);
        

          
        })
        .catch((error) => {
          console.error(error);
        });

    }
    });
}

    toggleCheckbox(checked, index)
    {
        console.log(checked);
        console.log(index);
        var data = this.state.answersCL;
        data[index].checked = checked;

        this.setState({answersCL: data});
    }
    onSelect(item, index)
    {
        //console.log(item);
        var data = this.state.answers;
        data[index].answer = item.option;
        
       
   
        //console.log(data);

    };

    onChangeText(text,index,type)
    {
        if(type == 1)
        {
            console.log(index)
            var data = this.state.answers;
            data[index].answer = text;
        }
        else if(type == 2)
        {
            var data = this.state.answersCL;
            data[index].answer = text;
        }
    
        //console.log(data);
    }
    

   
    renderQuestion()
    {
        var returnValue = [];
        // var data = [];

        // data.push({"event_topic_id":"123"});
        // data.push({"event_topic_id":"1235"});
        // data.push({"event_topic_id":"12356"});

        // console.log(data);
        // console.log(data[0]);

        // var x = {data: data};
        // console.log(JSON.stringify(x));
        
        var i = 0;

        this.state.questions.map ( question => {
            var index = i;
            var j = 0;
            if(question.type_answer == 'rb')
            {
                returnValue.push(
                    <View  key={index} style={styles.box}>
                <Text style={styles.text}>{question.question}</Text>
                
                <RadioForm
                  style={{ width: 350 - 30 }}
                  dataSource={question.answer}
                  itemShowKey="option"
                  itemRealKey="option"
                  circleSize={16}
                  initial={-1}
                  labelHorizontal={true}
                  onPress={(item) => this.onSelect(item, index)}
              />
                </View>
    )
            }
            else if(question.type_answer == 'cl')
            {
                
                // returnValue.push(
                // <View  key={index} style={styles.box}>
                //     <Text style={styles.text}>{question.question}</Text>);
                
                var checkBox = [];
                
                this.state.answersCL.map(answer => {
                    var x = j;
                    if(answer.type == 1)
                    {
                        console.log("checklist question");
                       
                        checkBox.push(<CheckBox key={x} onPress={(item) => this.toggleCheckbox(!answer.checked, x)}  title={answer.answer} checked={answer.checked} containerStyle={{borderColor: 'white', backgroundColor: 'white'}}/>)
                        
                    }
                    else if(answer.type == 2)
                    {
                        checkBox.push(<CheckBox key={x} onPress={(item) => this.toggleCheckbox(!answer.checked, x)}  title={"Lainnya"} checked={answer.checked} containerStyle={{borderColor: 'white', backgroundColor: 'white'}}/>)
                        checkBox.push(<TextInput 
                        style={{height: 40}}
                        placeholder="Write your answer here..."
                        multiline
                        onChangeText={(text) => this.onChangeText(text, x, 2)}/>
                        )
                    }
                    
                    j++;
                    //index++;
                });

                returnValue.push(<View key={index} style={styles.box}>
                    <Text style={styles.text}>{question.question}</Text>
                    {checkBox}
                    </View>
                    );
            
            }
            else if(question.type_answer == 'ft')
            {
                returnValue.push(
                    <View  key={index}  style={styles.box}>
                    <Text style={styles.text}>{question.question}</Text>
                    <TextInput
                    style={styles.essay} 
                              placeholder="Write your answer here..."
                  multiline
                  onChangeText={(text) => this.onChangeText(text, index, 1)}
                />
                
                    </View>
        
                )
            }
            
            if(question.type_answer != 'cl')
            i++;
        });

        //this.setState({ answers: data });

        return returnValue;

    //     this.state.questions.map ( question => {
    //         returnValue.push(
    //             <View style={styles.box}>
    //         {/* <Text style={styles.text}>{question.question}</Text>
    //         <RadioForm
    //           style={{ width: 350 - 30 }}
    //           dataSource={mockData}
    //           itemShowKey="label"
    //           itemRealKey="value"
    //           circleSize={16}
    //           initial={-1}
    //           labelHorizontal={true}
    //           onPress={(item) => this.onSelect(item)}
    //       /> */}
    //         </View>
    //         );
    //         console.log(question.question);
    //         question.answer.map( answer => {
    //             console.log(answer.option);
    //         })
    //   });

      //return returnValue;
    // );
    }

    onPressButton() {
         var data = this.state.answers;
         var dataCL = this.state.answersCL;

         var postData = {data: data};
       
        //  let request = {
        //     method: 'POST',
        //     body: JSON.stringify({"email": "asda"}),
        //     headers: {
        //       'Content-Type': 'application/json',
        //     } 
        //   }

        // console.log(JSON.stringify(
        //     postData
        //  ));

  

        status = true;


         this.state.answersCL.map(answer => {
             if(answer.checked == true)
             {
                data.push({"event_topic_id":answer.event_topic_id, "event_feedbacks_id": answer.event_feedbacks_id, "answer": answer.answer });
                
             }
         

            // dataCL.push({"event_topic_id":this.state.idtrack, "event_feedbacks_id": question.id, "answer": answer.option, "checked": false, "type" : type});

         });

         console.log(JSON.stringify(
            this.state.answers
         ));

       
         console.log("status 1");
         console.log(status);

         this.state.questions.map(question => {

             status1 = false;
             this.state.answers.map(answer => {
                if(answer.event_feedbacks_id == question.id)
                {
                    status1 = true;
                }
             })

             this.state.answersCL.map(answer => {
                if(answer.event_feedbacks_id == question.id)
                {
                    status1 = true;
                }
             })


             status = status1;

             

           
        

           // dataCL.push({"event_topic_id":this.state.idtrack, "event_feedbacks_id": question.id, "answer": answer.option, "checked": false, "type" : type});

        });

        console.log("status 2");
         console.log(status);

        this.state.answers.map(answer => {
            if(answer.answer == '' )
            {
                status = false;
            }
         });

         console.log("status 3");
         console.log(status);
         if(this.state.answered)
         {
            Alert.alert("Feedback already given.")
         }
         
         if(status && !this.state.answered)
         {
            fetch(urlDev + "/api/v1/feedback", {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.id
                },
                body: JSON.stringify(
                   postData
                ),
                }).then((resp) => {
                    if (resp.status === 200) {
                        return resp.json()
                    }
                    else if (resp.status === 401) {
                        Alert.alert('Message ', 'Access is denied')
                    } else if (resp.status === 503) {
                        Alert.alert('Message ', 'Service Unavailable')
                    }
                }).then((json) => {
                    Alert.alert('', json.message)
                    this.props.navigation.navigate('Home');

                    console.log(json.success);
                });
         }
         else
         {
             console.log("ada yang kosong");
             Alert.alert("Please answer all the question");
         }
          
          } 
          
    


    render() {
        if (this.state.isLoading) {
            return (
              <View style={{ flex: 1, padding: 20 }}>
                <ActivityIndicator />
              </View>
            )
          }

        return (
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.container1}>
            
            {this.renderQuestion()}

            
            <TouchableOpacity   onPress={() => this.onPressButton()} style={styles.submitbox}>
            <Text style={{fontSize:15, color:'#fff'}}>Submit Feedback</Text>

            </TouchableOpacity>

            </View>
            </ScrollView>
            
        )
        
    }

    


}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        
    },
    container1: {
        flexDirection: 'column',
        alignItems: 'center',
        padding: 10
    },
    box: {
        alignItems: 'flex-start',
        justifyContent:'center',
        backgroundColor: "#ffffff",
        
        width: '100%',
        padding:10,
        marginTop:10,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 3,
      },
      box1: {
        backgroundColor: "#ffffff",
        padding:10,
        marginTop:10,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 3,
      },
      submitbox: {
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: "#F7941D",
        width: '100%',
        padding:10,
        marginTop:10,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 3,
      },
     
    text: {
        color : '#9c9c9c',
        fontSize: 15
    },

    essay: {
        height : 150,
        textAlignVertical: 'top'
    }
})

