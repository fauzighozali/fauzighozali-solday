import React, { Component } from 'react'
import { View, Image, AsyncStorage, StatusBar, Dimensions } from 'react-native'


const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width;
// const jwtDecode = require('jwt-decode');

export default class SplashScreen extends Component {
  
  constructor() {
    super()
    this.state = {
      token: '',
      role: '',
    }
    AsyncStorage.getItem('token', (error, result) => {
      if (result) {
        this.setState({
          token: result
        });
      }
    });
    AsyncStorage.getItem('role', (error, result2) => {
      if (result2) {
        this.setState({
          role: result2
        });
        console.warn(result2)
      }
    });
  }



  render() {
    // let decodeToken = jwtDecode(this.state.token)
    // console.warn(decodeToken)
    setTimeout(() => {
      if (this.state.token !== '') {
        if (this.state.role !== 'eo') {
          this.props.navigation.navigate('Drawer',{
            token:this.state.token
          })
        } else {
          this.props.navigation.navigate('EventOrganizer'
          ,{
            dataEO:this.state.token
          }
          )
        }
      } else {
        this.props.navigation.navigate('Login')
      }
    }, 2000);

    return (
      <View style={{ justifyContent: 'center', alignItems: 'center' }}>
        <StatusBar backgroundColor={'transparent'} translucent />
        <Image style={{ height: '100%', width: imageWidth }} source={require('../../Images/bg_splash_screen.png')} />
      </View>
    )
  }
}