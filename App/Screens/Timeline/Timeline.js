import * as React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Timeline from 'react-native-timeline-flatlist'



var data = [
  {time: '08.00 – 09.00', title: 'Registration and Coffee Break', description: ''},
  {time: '09.00 – 09.05', title: 'Opening', description: ''},
  {time: '09.05 – 09.15', title: 'Welcome Speech', description: 'by Hendrix Pramana, President Director of AGIT'},
  {time: '09.15 – 09.55', title: 'Keynote Presentation : Integrated and Smart Industry (Industry 4.0)', description: 'by Shirley Santoso, President Director, PT A.T. Kearney'},
  {time: '09.55 – 10.35', title: 'Panel Discussion:', description: 'Moderator by Widi Triwibowo, Director of AGIT Guest Panelist: Shirley Santoso, President Director, PT A.T. Kearney, Panji Wasmana Technical Leader, IBM Indonesia'},
  {time: '10.35 – 11.05', title: 'Intelligent Enterprise - How to move in Industry 4.0', description: 'by Fariz Ikhromy Rahmansyah,Value Advisor Digital Transformation, Southeast Asia of SAP Indonesia'},
  {time: '11.05 – 11.35', title: 'Leveraging Security as an Advantage: Security to Delight and Retain Your Customer', description: 'by Shahnawaz Soomro, Advisor Strategy Solution, Broadcom Inc.'},
  {time: '11.35 – 11.45', title: 'Lucky Draw', description: ''} ,
  {time: '11.45 – 13.00', title: 'Lunch, Booth Tour, Networking', description: ''}

]

var data1 = [
  {time: '13.00 – 13.45', title: 'Driving Operational Excellence with the Autonomous Data Center (AI)', description: 'by Alvin Wahyudi, Nimble Sales Specialist, HP Enterprise'},
  {time: '13.45 – 14.30', title: 'Insight Sharing', description: 'by Vinita Pitoomal (Mulani) Corporate Sales Manager - Linkedin Indonesia'},
  {time: '14.30 – 15.15', title: 'People Management Solution on Hybrid Cloud', description: 'by Rizaldi Halim Hadian, Head of Owned Solution Business Development Dept, AGIT'},
  {time: '15.15 – 15.30', title: 'Closing, Prizes for 3 best point, Lucky Draw & Coffee Break – Booth Tour', description: ''}
]

var data2 = [
 
  {time: '13.00 – 13.45', title: 'IBM', description: 'by Henry Manullang, Country Manager Storage Solution, IBM Systems, IBM Indonesia'},
  {time: '13.45 – 14.30', title: 'Insight Sharing', description: 'by Mohit Gandas, Country Manager, RedDoorz for Indonesia'},
  {time: '14.30 - 15.15', title: 'Factory of the Future', description: 'by Octario Rezkavianto, IoT Product Manager, AGIT'},
  {time: '15.15 – 15.30', title: 'Closing, Prizes for 3 best point, Lucky Draw & Coffee Break – Booth Tour', description: ''}
]

var data3 = [

  {time: '13.00 – 13.45', title: 'Full-Stack Cloud Accelerates Industry Cloudiﬁcation and Innovation', description: 'by E. Wiryadi Salim, Solution Manager, Enterprise Solution Sales Dept, Huawei Tech Investment'},
  {time: '13.45 – 14.30', title: 'Insight Sharing', description: ''},
  {time: '14.30 - 15.15', title: 'Smart Connectivity towards Industry 4.0', description: 'by Agiek Pujo Trianto Smart Connectivity Technology Architect, AGIT'},
  {time: '15.15 – 15.30', title: 'Closing, Prizes for 3 best point, Lucky Draw & Coffee Break – Booth Tour', description: ''}
]

  
  FirstRoute = () => <View style={styles.container}><Timeline circleSize={20}
  circleColor='rgb(54,184,245)'
  lineColor='rgb(45,156,219)'
  timeContainerStyle={{minWidth:52}}
  timeStyle={{textAlign: 'center', backgroundColor:'#ff9797', color:'white', padding:5, borderRadius:13}}
  descriptionStyle={{color:'gray'}}
  style={styles.flexContainer}data={data}></Timeline></View>;

  SecondRoute = () => <View style={styles.container}><Timeline circleSize={20}
  circleColor='rgb(248,146,35)'
  lineColor='rgb(45,156,219)'
  timeContainerStyle={{minWidth:52}}
  timeStyle={{textAlign: 'center', backgroundColor:'#ff9797', color:'white', padding:5, borderRadius:13}}
  descriptionStyle={{color:'gray'}}
  style={styles.flexContainer}data={data1}></Timeline></View>;

  ThirdRoute = () => <View style={styles.container}><Timeline circleSize={20}
  circleColor='rgb(21,95,158)'
  lineColor='rgb(45,156,219)'
  timeContainerStyle={{minWidth:52}}
  timeStyle={{textAlign: 'center', backgroundColor:'#ff9797', color:'white', padding:5, borderRadius:13}}
  descriptionStyle={{color:'gray'}}
  style={styles.flexContainer}data={data2}></Timeline></View>;

  FourthRoute = () => <View style={styles.container}><Timeline circleSize={20}
  circleColor='rgb(131,21,70)'
  lineColor='rgb(45,156,219)'
  timeContainerStyle={{minWidth:52}}
  timeStyle={{textAlign: 'center', backgroundColor:'#ff9797', color:'white', padding:5, borderRadius:13}}
  descriptionStyle={{color:'gray'}}
  style={styles.flexContainer}data={data3}></Timeline></View>;



export default class TimelineActivity extends React.Component {
    // static navigationOptions = {
    //     title: 'HomeScreen',
    //     header: null
    // };
 

  constructor(props) {
      super(props);
      this.state = {
        index: 0,
        routes: [
          { key: 'maintrack', title: 'Main' },
          { key: 'track1', title: 'Track 1' },
          { key: 'track2', title: 'Track 2' },
          { key: 'track3', title: 'Track 3' }
        ],
      };

  //     data = [
  //   {time: '09:00', title: 'Event 1', description: 'Event 1 Description' ,imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240340/c0f96b3a-0fe3-11e7-8964-fe66e4d9be7a.jpg'},
  //   {time: '10:45', title: 'Event 2', description: 'Event 2 Description'},
  //   {time: '12:00', title: 'Event 3', description: 'Event 3 Description'},
  //   {time: '14:00', title: 'Event 4', description: 'Event 4 Description'},
  //   {time: '16:30', title: 'Event 5', description: 'Event 5 Description'}
  // ]


  }

  

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = props => <TabBar  {...props} />;

  _renderScene = SceneMap({
    maintrack: FirstRoute,
    track1: SecondRoute,
    track2: ThirdRoute,
    track3: FourthRoute
  });

  _renderLabel(scene) {
    const { getLabelText } = this;
    const label = getLabelText(scene);

    return (
      <Text style={styles.content}>
       
        {label}
      </Text>
    );
  }


  renderDetail(rowData, sectionID, rowID) {
    console.log("timeline");
  }
  
  render() {
      
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        
        onIndexChange={this._handleIndexChange}
        renderTabBar={props => (
          <TabBar
            {...props}
            renderLabel={this._renderLabel}
            getLabelText={({ route: { title } }) => title}
            indicatorStyle={styles.indicator}
            tabStyle={styles.tabStyle}
            style={styles.tab}
          />
        )}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
    flex: 1,
    flexDirection: 'row',
    justifyContent:'center'
  },
  flexContainer: {
    flexDirection: 'row',
    marginLeft: 0,

},
tab: {
  backgroundColor: 'white',
},
indicator: {
  backgroundColor: '#f5b969',
},
content: {
  padding:10,
  color: '#f5b969',
  fontSize: 15,
  fontWeight: 'bold'

}

});