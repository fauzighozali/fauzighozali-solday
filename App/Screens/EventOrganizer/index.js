import React, { Component } from 'react';
import { Image, Text, View, Alert, StyleSheet, AsyncStorage, StatusBar, ActivityIndicator } from 'react-native';
import { urlDev } from '../../lib/server'

import { ButtonOrange } from '../../Components/PlaceButton'
import { DialogMessage } from '../../Components/DialogInput'

const jwtDecode = require('jwt-decode')

class EOScreen extends Component {

    state = {
        token: '',
        isLoading: true,
        responseData: '',
        isShowDialog: false,
    }

    componentDidMount() {
        const { navigation } = this.props;
        const data = navigation.getParam('dataEO');

        AsyncStorage.getItem('token', (error, result) => {
            if (result) {
                this.setState({
                    token: result
                });
                this.setState({
                    responseData: jwtDecode(result)
                })
            } else {

                this.setState({
                    responseData: jwtDecode(data)
                })
            }
        })

    }

     render() {

        // if (this.state.isLoading) {
        //     return (
        //         <View style={{ flex: 1, padding: 20 }}>
        //             <ActivityIndicator />
        //         </View>
        //     )
        // }

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <View style={styles.detailProfile}>
                    <Image
                        style={{ width: 100, height: 100, borderRadius: 50, marginTop: 10, marginBottom: 5 }}
                        source={require('../../Images/avatar_profile.png')} />
                    <Text style={{ fontSize: 18, color: '#000' }}>{this.state.responseData.name}</Text>
                    <Text style={{ fontSize: 14, color: '#000', fontStyle: 'italic' }}>{this.state.responseData.job_title}</Text>
                    <Text style={{ fontSize: 16, color: '#000' }}>{this.state.responseData.corporate}</Text>
                </View>
                <View style={styles.buttonContainer}>
                    <ButtonOrange
                        title={'Scan QR Code'}
                        onPress={() => this.props.navigation.navigate('Scanner')}
                    />
                    <ButtonOrange
                        title={'Log out'}
                        onPress={() => {
                            this.setState({
                                isShowDialog: true
                            })
                        }}
                    />
                </View>
                <DialogMessage
                    visible={this.state.isShowDialog}
                    description={'You are exiting this account?'}
                    close={() =>
                        this.setState({
                            isShowDialog: false
                        })
                    }
                    submit={() => {
                        AsyncStorage.clear()
                        AsyncStorage.setItem('token', '')
                        AsyncStorage.setItem('role', '')
                        this.props.navigation.navigate('Login')
                        this.setState({
                            isShowDialog: false
                        })
                    }}
                />
            </View>

        )

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
    },
    detailProfile: {
        borderBottomWidth: 1,
        borderColor: '#ddd',
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 16,
        paddingBottom: 5,
        alignItems: 'center'
    },
    buttonContainer: {
        flex: 1,
        justifyContent: 'center',
        padding: 16,
    }
})

export default EOScreen
