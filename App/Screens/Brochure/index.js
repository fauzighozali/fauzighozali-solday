import React, { Component } from 'react';
import {
    FlatList,
    Linking,
    View,
    Text,
    StyleSheet,
    ScrollView,
    Image,
    TouchableOpacity,
    AsyncStorage,
} from 'react-native';

import { urlDev } from '../../lib/server'
import ListItem from '../../Components/ListItem'

class BrochureScreen extends Component {


    static navigationOptions =
        {
            title: 'BrochurScreen',
        };

    constructor() {
        super()
        this.state = {
            responseData: [],
        }
        AsyncStorage.getItem('token', (error, result) => {
            if (result) {
                this.setState({
                    token: result
                });
            }

            return fetch(urlDev + '/api/v1/brochure?filter=[{"property":"event_brochures.category","value":"topic","operator":"like"}]&sort=[{"property":"event_brochures.title","direction":"ASC"}]', {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + result
                },
            })
                .then((resp) => {
                    // console.warn(resp)
                    this.setState({
                        isLoading: false
                    })

                    if (resp.status === 200) {
                        return resp.json()
                    }
                    else if (resp.status === 401) {
                        Alert.alert('Message ', 'Access is denied')
                    } else if (resp.status === 503) {
                        Alert.alert('Message ', 'Service Unavailable')
                    }
                })
                .then((json) => {
                    // console.warn(json)
                    if (json.success == true) {
                        this.setState({
                            responseData: json.rows
                        }, function () {

                        })
                    }
                })
                .done()
        })

    }

    render() {
        // const {navigate} = this.props.navigation;

        return (

            < View style={styles.container} >
                <FlatList
                    data={this.state.responseData}
                    renderItem={({ item }) =>
                        <TouchableOpacity onPress={() => {
                            Linking.openURL(item.url).catch(err => console.error('An error occurred', err));
                        }}>
                            <View style={styles.listItem}>
                                <Text style={{ fontSize: 16 }}>
                                    {item.title}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    } keyExtractor={({ id }, index) => id}
                />
            </View >

        )

    }




}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10,
        backgroundColor: '#fff'
    },
    text: {
        color: '#9c9c9c',
        fontSize: 20
    },
    listItem: {
        padding: 10,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 2,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 16,
        marginRight: 16,
        marginBottom: 5
    },
})

export default BrochureScreen
