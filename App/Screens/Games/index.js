import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, AsyncStorage, FlatList, ImageBackground, TouchableOpacity, } from 'react-native'
import { ButtonOrange } from '../../Components/PlaceButton'
import { urlDev } from '../../lib/server';

export default class GamesScreen extends Component {

    state = {
        token: '',
        isLoading: true,
        responseData: '',
        myData: '',
        message: ''
    }

    constructor() {
        super()
        AsyncStorage.getItem('token', (error, result) => {
            if (result) {
                this.setState({
                    token: result
                });
                console.warn(this.state.token)
                this.getTopPoint()
            }
        })
    }

    getTopPoint = () => {
        return fetch(urlDev + '/api/v1/leaderboard', {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.token
            },
        }).then((resp) => {

            if (resp.status === 200) {
                return resp.json().then((json) => {
                    console.warn(json)
                    if (json.success === true) {
                        this.setState({
                            responseData: json.rows,
                            myData: json.my_rank[0]
                        })
                    } else {
                        console.warn(json.message)
                    }
                })
            }
            else if (resp.status === 401) {
                this.getTopPoint()
            } else if (resp.status === 503) {
                Alert.alert('Message ', 'Service Unavailable')
            }
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ImageBackground animationType="slide" style={styles.container} source={require('../../Images/bg_splash_screen.png')}>
                    <Text style={styles.textTitle}>TOP POINT</Text>
                    <View style={styles.poinContent}>
                        <View style={styles.baris}>
                            <Text style={{ marginBottom: 10, fontWeight: 'bold', color: 'orange' }}>Rank</Text>
                            <Text style={{ marginBottom: 10, fontWeight: 'bold', color: 'orange' }}>Name</Text>
                            <Text style={{ marginBottom: 10, fontWeight: 'bold', color: 'orange' }}>Point</Text>
                        </View>
                        <View style={styles.listRank}>
                            <FlatList
                                data={this.state.responseData}
                                renderItem={({ item }) =>
                                    <View style={styles.baris}>
                                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>{item.rank}</Text>
                                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>{item.name}</Text>
                                        <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>{item.point}</Text>
                                    </View>

                                } keyExtractor={({ id }, index) => id}
                            />
                        </View>
                        <View style={{ backgroundColor: 'orange', height: 0.5, width: '100%', marginBottom: 10, marginTop: 20 }} />
                        <View style={styles.baris}>
                            <Text style={{ marginBottom: 10, fontWeight: 'bold', color: 'orange' }}>{this.state.myData.rank}</Text>
                            <Text style={{ marginBottom: 10, fontWeight: 'bold', color: 'orange' }}>{this.state.myData.name}</Text>
                            <Text style={{ marginBottom: 10, fontWeight: 'bold', color: 'orange' }}>{this.state.myData.point}</Text>
                        </View>
                    </View>
                    <View style={styles.menus}>
                        <ButtonOrange
                            title='PLAY'
                            onPress={() => {
                                this.props.navigation.navigate('ScannerGames')
                            }}
                        />
                    </View>
                    <TouchableOpacity onPress={() => {
                        console.warn('tes')
                        this.getTopPoint()
                    }}
                        style={styles.floatButton}>
                        <Image
                            source={require('../../Images/reload.png')}
                            style={styles.imageFloat}
                        />
                    </TouchableOpacity>
                </ImageBackground>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textTitle: {
        fontSize: 20,
        color: '#fff',
        fontWeight: '900',
        marginBottom: 5,
        fontStyle: 'italic',
        justifyContent: 'center'
    },
    listRank: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    poinContent: {
        width: '80%',
        height: 270,
        borderRadius: 10,
        backgroundColor: '#FFF',
        padding: 16,
        marginBottom: 16,
    },
    menus: {
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    baris: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    floatButton: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 16,
        bottom: 16,
      },
      imageFloat: {
        height: 60,
        width: 60,
      }


})