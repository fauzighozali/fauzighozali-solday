import React, { Component } from 'react'
import {
    View, 
    StyleSheet,
    Dimensions,
    AsyncStorage,
} from 'react-native'

import {urlDev} from '../../lib/server'
import QRCodeScanner from 'react-native-qrcode-scanner'
import {DialogMessage2} from '../../Components/DialogInput'

export default class ScannerGamesScreen extends Component {


    state = {
        isShowDialog: false,
        message: ''
    }

    constructor() {
        super()

        AsyncStorage.getItem('token', (error, result) => {
            if (result) {
                this.setState({
                    token: result
                });
            }
        });

    }
  

    onSuccess = (e) => {
        return fetch(urlDev + '/api/v1/game', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.token
            },
            body: JSON.stringify({
                'qrcode': e.data
            })
        }).then((resp) => {
            console.warn(resp)
            if (resp.status === 200) {
                return resp.json()
            }
            else if (resp.status === 401) {
                Alert.alert('Message ', 'Access is denied')
            } else if (resp.status === 503) {
                Alert.alert('Message ', 'Service Unavailable')
            }
        }).then((json) => {
            console.warn(json)

            if (json.success === true) {
                let message = json.message

                this.setState({
                    isShowDialog: true,
                    message: message
                })
            } else {
                this.setState({
                    isShowDialog: true,
                    message: json.message,
                })
            }
        }).done();

    }


    render() {
        return (
            <View style={styles.container}>
                <QRCodeScanner
                    onRead={this.onSuccess}
                    showMarker
                    ref={(node) => { this.scanner = node }}
                    cameraStyle={styles.cameraContainer}
                    topViewStyle={styles.zeroContainer}
                    bottomViewStyle={styles.zeroContainer}
                />
                 <DialogMessage2
                    visible={this.state.isShowDialog}
                    description={
                        this.state.message
                    }
                    submit={() => {
                        this.scanner.reactivate()
                        this.setState({ isShowDialog: false })
                    }}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    zeroContainer: {
        height: 0,
        flex: 0,
    },
    cameraContainer: {
        height: Dimensions.get('window').height,
    },
})