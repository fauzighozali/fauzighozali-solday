import React, { Component } from 'react'
import {
    StyleSheet,
    Picker,
    Text,
    View,
    AsyncStorage,
    Alert,
    Dimensions
} from 'react-native'


import QRCodeScanner from 'react-native-qrcode-scanner'
import { urlDev } from '../../lib/server';
import { DialogMessage2 } from '../../Components/DialogInput'


const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;


export default class ScannerScreen extends Component {

    state = {
        track: 'in',
        isShowDialog: false,
        message: ''
    }
    constructor() {
        super()

        AsyncStorage.getItem('token', (error, result) => {
            if (result) {
                this.setState({
                    token: result
                });
            }
        });

    }
    updateTrack = (t) => {
        this.scanner.reactivate()
        this.setState({ track: t })
    }

    onSuccess = (e) => {

        return fetch(urlDev + '/api/v1/attendance/' + this.state.track, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.token
            },
            body: JSON.stringify({
                'qrcode': e.data
            })
        }).then((resp) => {

            if (resp.status === 200) {
                return resp.json()
            }
            else if (resp.status === 401) {
                Alert.alert('Message ', 'Access is denied')
            } else if (resp.status === 503) {
                Alert.alert('Message ', 'Service Unavailable')
            }
        }).then((json) => {
            console.warn(json)

            if (json.success === true) {
                let account = json.account

                this.setState({
                    isShowDialog: true,
                    message: account.name + '\n' + account.job_title + '\n' + account.corporate + '\n',
                })
                // Alert.alert(
                //     '',
                //     account.name + '\n' +
                //     account.job_title + '\n' +
                //     account.corporate + '\n',
                //     [
                //         { text: 'OK', onPress: () => this.scanner.reactivate() },
                //     ],
                //     { cancelable: false },
                // );
            } else {
                this.setState({
                    isShowDialog: true,
                    message: e.data + '\n' + json.errors,
                })
                // Alert.alert(
                //     '',
                //     json.errors,
                //     [
                //         { text: 'OK', onPress: () => this.scanner.reactivate() },
                //     ],
                //     { cancelable: false },
                // );
            }
        }).done();

    }


    render() {
        // console.warn('token: '+this.state.track)
        return (
            <View style={styles.container}>

                <View style={styles.selectPlace}>
                    <Text style={{ fontSize: 16, }}>Select the type of presence</Text>
                    <Picker
                        mode='dropdown'
                        style={{ height: 30, width: '100%', padding: 16 }}
                        selectedValue={this.state.track}
                        onValueChange={this.updateTrack}>
                        <Picker.Item label="Main: AGIT Solution Day 2019" value="in" />
                        <Picker.Item label="Track 1: Next Generation Infrastructure" value="track_1" />
                        <Picker.Item label="Track 2: Supply Chain, Logistic & Distribution" value="track_2" />
                        <Picker.Item label="Track 3: Talent Management & Workforce Productivity" value="track_3" />
                        <Picker.Item label="Get Souvenir" value="souvenir" />
                    </Picker>
                </View>
                <View style={{ flex: 1 }}>
                    <QRCodeScanner
                        onRead={this.onSuccess}
                        showMarker
                        ref={(node) => { this.scanner = node }}
                    />
                </View>
                <DialogMessage2
                    visible={this.state.isShowDialog}
                    description={
                        this.state.message
                    }
                    submit={() => {
                        this.scanner.reactivate()
                        this.setState({ isShowDialog: false })
                    }}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    selectPlace: {
        height: '20%',
        alignItems: 'flex-start',
        padding: 16
    }
})