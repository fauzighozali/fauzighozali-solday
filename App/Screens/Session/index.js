import React, { Component } from 'react'
import { StyleSheet, ImageBackground, View, Text, Image, Modal, AsyncStorage, Dimensions } from 'react-native'
import { PlaceButton } from '../../Components/PlaceButton'
import { DialogMultiInput, DialogMessage2 } from "../../Components/DialogInput"
import { urlDev } from '../../lib/server';

import Loader from '../../Components/Loader';




class Session extends Component {

    state = {
        isDialogVisibale: false,
        question: '',
        token: '',
        loading: false,
        isShowDialog: false,
        message: ''
    }

    constructor() {
        super()
        AsyncStorage.getItem('token', (error, result) => {
            if (result) {
                this.setState({
                    token: result
                })
            }
        }
        )
    }


    updateQuestion = (question) => {
        this.setState({
            question: question
        })
    }

    showDialog = () => {
        this.setState({
            isDialogVisible: true
        })
    }

    hendleSubmit = (id) => {
        this.setState({
            loading: true
        })
        // this.props.navigation.navigate('Session');
        return fetch(urlDev + '/api/v1/question', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.token
            },
            body: JSON.stringify({
                "value": this.state.question,
                "event_topic_id": id
            }),
        }).then((resp) => {
            console.warn(resp)
            this.setState({
                isDialogVisible: false,
                loading: false
            })
            if (resp.status === 200) {
                return resp.json()
            }
            else if (resp.status === 401) {
                Alert.alert('Message ', 'Access is denied')
            } else if (resp.status === 503) {
                Alert.alert('Message ', 'Service Unavailable')
            }
        }).then((json) => {
            if (json.success == true) {
                this.setState({
                    isShowDialog: true,
                    message: json.message
                }, function () {

                })
            } else {
                this.setState({
                    isShowDialog: true,
                    message: json.message
                })
            }
        })
    }

    hendleCancel = () => {
        this.setState({
            isDialogVisible: false
        })
    }

    handlerQuestion = (s, t) => {
        console.warn('ques:' + s.is_question)

        if (s.is_question === 1) {
            this.props.navigation.navigate('Question', {
                speaker: s
            });
        } else if (s.is_question === 2) {
            this.setState({ isDialogVisible: true })
        }
    }

    render() {

        const { navigation } = this.props;
        const speaker = navigation.getParam('speaker');
        const track = navigation.getParam('track')

        const QuestionFeedback =
            <View style={{width:Dimensions.get('window').width, paddingLeft:16, paddingRight:16}}>
                <PlaceButton
                    title={'Feedback'}
                    onPress={() => {
                        this.props.navigation.navigate('Feedback', {
                            speaker: speaker
                        });
                    }}
                />

                <PlaceButton
                    title={'Question'}
                    onPress={() => {
                        this.handlerQuestion(speaker, track)
                    }}
                />
            </View>



        const Feedback =

            <PlaceButton
                title={'Feedback'}
                onPress={() => {
                    this.props.navigation.navigate('Feedback', {
                        speaker: speaker
                    });
                }}
            />


        const Question =
            <PlaceButton
                title={'Question'}
                onPress={() => {
                    this.handlerQuestion(speaker, track)
                }}
            />

        // let profile_pic=JSON.stringify(speaker.profile_pic)
        // console.warn(profile_pic))
        let buttonView;
        if (speaker.is_question === 0) {
            buttonView = Feedback
        } else if (speaker.is_feedback === 0) {
            buttonView = Question
        } else {
            buttonView = QuestionFeedback
        }


        return (
            <ImageBackground animationType="slide" style={styles.container} source={require('../../Images/bg_splash_screen.png')}>
                <View style={styles.detailSpeaker}>
                    <Image
                        style={{ width: 100, height: 100, borderRadius: 50, marginBottom: 10, marginTop: 10 }}
                        source={{ uri: speaker.profile_pic }} />
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{speaker.name}</Text>
                    <Text style={{ fontSize: 14, fontStyle: 'italic' }}>{speaker.job_title}</Text>
                    <Text style={{ fontSize: 14, fontStyle: 'italic' }}>{speaker.corporate}</Text>
                    <Text style={{ fontSize: 12 }}>{speaker.description}</Text>
                </View>
                {buttonView}
                {/* <View style={styles.menus}> */}

                {/* <PlaceButton
                    title={'Feedback'}
                    onPress={() => {
                        this.props.navigation.navigate('Feedback', {
                            speaker: speaker
                        });
                    }}
                />

                <PlaceButton
                    title={'Question'}
                    onPress={() => {
                        this.handlerQuestion(speaker, track)
                    }}
                /> */}

                {/* </View> */}

                <DialogMultiInput
                    placeHolder={"Question..."}
                    updateQuestion={this.updateQuestion}
                    visible={this.state.isDialogVisible}
                    hendleCancel={this.hendleCancel}
                    hendleSubmit={() =>
                        this.hendleSubmit(speaker.id)
                    }
                />
                <Loader
                    title={"Loading"}
                    loading={this.state.loading} />

                <DialogMessage2
                    visible={this.state.isShowDialog}
                    description={
                        this.state.message
                    }
                    submit={() => {
                        this.setState({ isShowDialog: false })
                    }}
                />
            </ImageBackground>

        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 16,
    },
    detailSpeaker: {
        width: Dimensions.get('window').width - 32,
        borderRadius: 10,
        backgroundColor: '#FFF',
        padding: 16,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 16
    },
    menus: {
        justifyContent: 'center'
    },
    dialogConntainer: {
        padding: 5,
        textAlign: 'center',
        borderColor: '#F7941D',
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderRadius: 5
    },
})

export default Session;
