import React, { Component } from 'react'
import {
    ScrollView,
    StyleSheet,
    View,
    Text,
    Image,
    Alert,
    ActivityIndicator,
    AsyncStorage,
} from 'react-native'

import { ButtonOrange } from '../../Components/PlaceButton'
import { urlDev } from '../../lib/server'
import { DialogInput2, DialogMessage2 } from '../../Components/DialogInput'



class ProfileScreen extends Component {
    state = {
        id: '',
        isLoading: true,
        isShowDialog: false,
        isShowDialog2: false,
        message: '',
        responseData: '',
        oldPassword: '',
        changePassword: '',
        confirmPassword: '',

    }
    constructor() {
        super()
        AsyncStorage.getItem('token', (error, result) => {
            if (result) {
                this.setState({
                    id: result
                });
            }
        });
        AsyncStorage.getItem('oldPassword', (error, result) => {
            if (result) {
                this.setState({
                    oldPassword: result
                });
            }
        });

        this.getDataProfile()
    }

    getDataProfile = () => {
        this.setState({ isLoading: true })
        return fetch(urlDev + '/api/v1/profile', {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.id
            },
        })
            .then((resp) => {
                // console.warn(resp)
                this.setState({
                    isLoading: false
                })

                if (resp.status === 200) {
                    return resp.json().then((json) => {
                        console.warn(json)
                        if (json.success == true) {
                            this.setState({
                                responseData: json.rows[0]
                            }, function () {

                            })
                        } else {
                            // Alert.alert('Message ', json.message)
                        }
                    })
                }
                else if (resp.status === 401) {
                    this.setState({ isLoading: true })
                    this.getDataProfile()
                } else if (resp.status === 503) {
                    Alert.alert('Message ', 'Service Unavailable')
                }
            }).done();
    }

    updatePassword = (p) => {
        this.setState({
            changePassword: p
        })
    }
    updatePassword2 = (p) => {
        this.setState({
            confirmPassword: p
        })
    }

    handlerSubmit = () => {
        // this.setState({ isShowDialog: false, isLoading: true })
        if (this.state.changePassword === this.state.confirmPassword) {
            console.warn(this.state.id)
            return fetch(urlDev + '/api/v1/changepassword', {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.id
                },
                body: JSON.stringify({
                    "old_password": this.state.oldPassword,
                    "new_password": this.state.changePassword
                })
            })
                .then((resp) => {
                    console.warn(resp)
                    this.setState({
                        isLoading: false,
                        isShowDialog:false,
                    })

                    if (resp.status === 200) {
                        return resp.json().then((json) => {
                            // console.warn(json)
                            if (json.success == true) {
                                AsyncStorage.setItem('oldPassword', this.state.changePassword)
                                this.setState({
                                    isShowDialog2: true,
                                    message: json.message
                                }, function () {

                                })
                            } else {
                                // Alert.alert('Message ', json.message)
                            }
                        })
                    }
                    else if (resp.status === 401) {
                        // this.setState({ isLoading: true })
                        // this.getDataProfile()
                        console.warn('requst_salah')
                        this.handlerSubmit()
                    } else if (resp.status === 503) {
                        Alert.alert('Message ', 'Service Unavailable')
                    }
                }).done();
        } else {
            this.setState({
                isShowDialog2: true,
                message: 'Your password and confirmation password do not match'
            })

        }
    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    <ActivityIndicator />
                </View>
            )
        }

        return (
            <ScrollView>
                <View style={styles.detailProfile}>
                    <Image
                        style={{ width: 80, height: 80, marginTop: 10, marginBottom: 5 }}
                        source={require('../../Images/avatar_profile.png')} />
                    <Text style={{ fontSize: 18, color: '#000' }}>{this.state.responseData.name}</Text>
                    <Text style={{ fontSize: 14, color: '#000', fontStyle: 'italic' }}>{this.state.responseData.job_title}</Text>
                    <Text style={{ fontSize: 16, color: '#000' }}>{this.state.responseData.corporate}</Text>
                    <Text style={{ fontSize: 16, color: '#F7941D' }}>My Point : {this.state.responseData.point}</Text>
                </View>
                <View style={styles.qrContainer}>
                    <Image style={styles.qrcode} source={{ uri: "https://api.qrserver.com/v1/create-qr-code/?data=" + this.state.responseData.mst_barcode_code }} />
                </View>
                <View style={{ flex: 1, AlignItems: 'flex-end', justifyContent: 'flex-end', padding: 16 }}>
                    <ButtonOrange title={"Go To Track"}
                        onPress={() => {
                            this.props.navigation.navigate('Track')
                        }} />
                    <ButtonOrange
                        title={'Change Password'}
                        onPress={() => this.setState({ isShowDialog: true })}
                    />
                </View>
                <DialogInput2
                    visible={this.state.isShowDialog}
                    placeHolder={'New password'}
                    placeHolder2={'Confirm password'}
                    updateCode={this.updatePassword}
                    updateCode2={this.updatePassword2}
                    hendleCancel={() => this.setState({ isShowDialog: false })}
                    hendleSubmit={this.handlerSubmit}
                />
                <DialogMessage2
                    visible={this.state.isShowDialog2}
                    description={
                        this.state.message
                    }
                    submit={() => {
                        this.setState({ isShowDialog2: false })
                    }}
                />
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    detailProfile: {
        borderBottomWidth: 1,
        borderColor: '#ddd',
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 16,
        paddingBottom: 5,
        alignItems: 'center',
    },
    qrContainer: {
        alignItems: 'center',
        padding: 16,
    },
    qrcode: {
        marginTop: 20,
        height: 200,
        width: 200,
        padding: 50,
        backgroundColor: '#ececec',
        borderColor: 'red',
        borderWidth: 1,
    }
})

export default ProfileScreen
