import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    TextInput,
    ImageBackground,
    AsyncStorage,
    Dimensions,
    Alert,
    BackHandler,
    Platform,
    StatusBar,
    KeyboardAvoidingView
} from 'react-native'
import { ButtonOrange } from '../../Components/PlaceButton'
import Loader from "../../Components/Loader"

import { urlDev } from '../../lib/server'
import { DialogMessage2 } from '../../Components/DialogInput'
import { saveSessionForLocal } from '../../Actions/Session'

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width;
const jwtDecode = require('jwt-decode');
class LoginScreen extends Component {
    constructor() {
        super()
        AsyncStorage.clear
        this.state = {
            email: '',
            password: '',
            validasi: false,
            netInfo: true,
            loading: false,
            responseData: '',
            status: 'admo',
            validasi: false,
            message: '',
            isShowDialog: false,
            token: ''
        }

        AsyncStorage.setItem('token', '')
        AsyncStorage.setItem('role', '')
    }

    updateData = (value, field) => {
        if (field == 'email') {
            this.setState({
                email: value
            })
        } else if (field == 'password') {
            this.setState({
                password: value
            })
        }
    }

    componentDidUpdate(prevState){
        if(prevState.status !== this.state.status){
            this.loginAction
        }
    }

    loginAction = () => {
        console.warn(this.state.email + ' ' + this.state.password)
        this.textInputRef.clear();
        if (this.state.password !== null && this.state.email !== null) {
            this.setState({
                validasi: true,
            })
            if (this.state.netInfo === true) {
                this.setState({
                    loading: true
                })
                return fetch(urlDev + '/api/v1/login', {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        "email": this.state.email,
                        "password": this.state.password
                    }),

                }).then((resp) => {
                    // console.warn(resp)
                    this.setState({
                        loading: false
                    })

                    if (resp.status === 200) {
                        return resp.json()
                    }
                    else if (resp.status === 401) {
                        Alert.alert('Message ', 'Access is denied')
                    } else if (resp.status === 503) {
                        Alert.alert('Message ', 'Service Unavailable')
                    }
                })
                    .then((json) => {
                        // console.warn(json)
                        if (json.success == true) {
                            let token = json.token
                            let decodeToken = jwtDecode(token)
                            let roles = decodeToken.roles
                            var status='admo';

                            for (i = 0; i < roles.length; i++) {
                                if (roles[i] === 'eo') {
                                    this.setState({
                                        status: roles[i]
                                    })
                                    status=roles[i]
                                }
                            }

                            console.warn(AsyncStorage.getItem('role') + ' '+ status)

                            // console.warn(this.state.status)
                            if (status === 'admo') {
                                this.props.navigation.navigate('Drawer',{
                                    token:json.token
                                });
                            } else if (status === 'eo') {
                                this.props.navigation.navigate('EventOrganizer'
                                ,{
                                    dataEO: token
                                }
                                );
                            }

                            this.setState({
                                responseData: json
                            })

                            AsyncStorage.setItem('token', json.token)
                            AsyncStorage.setItem('oldPassword', this.state.password)
                            AsyncStorage.setItem('role', this.state.status)


                        } else if (json.success === false) {
                            this.setState({
                                isShowDialog: true,
                                message: json.message
                            })
                        }
                    })
                    .done();
            } else {
                this.setState({
                    isShowDialog: true,
                    loading: false,
                    message: json.message
                })
                // Alert.alert('No Internet Acces')
            }
        } else {
            this.setState({
                isShowDialog: true,
                validasi: false,
                message: 'Login failed. Worng username or password'
            })
        }

    }

    // close the app
    componentDidMount() {
        // if (Platform.OS == "android") {
        //     BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        // }
    }
    handleBackButton = () => {
        Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            },], {
                cancelable: false
            }
        )
        return true;
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }


    render() {
        return (
          <KeyboardAvoidingView style={{ flex: 1, justifyContent: 'flex-end', }} behavior="padding" enabled>
            <View style={{ flex: 1 }}>
                {/* <StatusBar backgroundColor="#fff" barStyle="dark-content" /> */}

                <StatusBar backgroundColor={'transparent'} translucent />
                <View style={{
                    flex: 1,
                }}>
                    <ImageBackground animationType="slide" style={styles.container} source={require('../../Images/bg_login.png')}>
                        <Loader
                            title={"Loading"}
                            loading={this.state.loading} />
                        <View style={styles.formContainer} >
                            <TextInput
                                placeholder='Email'
                                placeholderTextColor='grey'
                                style={styles.inputContainer}
                                onChangeText={(email) => this.updateData(email, 'email')}
                                value={this.state.email}
                            />
                            <TextInput
                                placeholder='Password'
                                placeholderTextColor='grey'
                                style={styles.inputContainer}
                                secureTextEntry={true}
                                ref={ref => this.textInputRef = ref}
                                value={this.state.password}
                                onChangeText={(password) => this.updateData(password, 'password')}
                            />
                            <ButtonOrange
                                onPress={
                                    this.loginAction
                                }
                                title='Login'
                            />
                        </View>
                    </ImageBackground>
                </View>
                <DialogMessage2
                    visible={this.state.isShowDialog}
                    description={
                        this.state.message
                    }
                    submit={() => {
                        this.setState({ isShowDialog: false })
                    }}
                />
            </View>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 16,
        width: imageWidth,
    },
    formContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 30,
    },
    inputContainer: {
      height: 45,
        borderWidth: 0.5,
        borderColor: "#F7941D",
        marginBottom: 10,
        borderRadius: 5,
        backgroundColor: '#FFF',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        paddingLeft: 12,
        paddingRight: 12,
    }
})

export default LoginScreen
