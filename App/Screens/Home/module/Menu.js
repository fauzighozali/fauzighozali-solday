import React from 'react'
import {StyleSheet, View, TouchableOpacity, Image, Text} from 'react-native'

const Menu =(props)=>(
    <View style={styles.manuLayout}>
        <View style={styles.menuItem}>
            <TouchableOpacity style={styles.menu} onPress={props.timeline}>
              <Image style={styles.menuIcon} source={require('../../../Images/agenda.png')} />
              <Text style={styles.menuTitle}>Agenda</Text>
            </TouchableOpacity>
        </View>
        
        <View style={styles.menuItem}>
           <TouchableOpacity style={styles.menu} onPress={props.games}>
              <Image style={styles.menuIcon} source={require('../../../Images/games.png')} />
              <Text style={styles.menuTitle}>Games</Text>
            </TouchableOpacity>
        </View>

        <View style={styles.menuItem}>
            <TouchableOpacity style={styles.menu} onPress={props.track}>
              <Image style={styles.menuIcon} source={require('../../../Images/track.png')} />
              <Text style={styles.menuTitle}>Questions and Track</Text>
            </TouchableOpacity>
        </View>

        <View style={styles.menuItem}>
            <TouchableOpacity style={styles.menu} onPress={props.brochure}>
              <Image style={styles.menuIcon} source={require('../../../Images/brochure.png')} />
              <Text style={styles.menuTitle}>Presentation Materials</Text>
            </TouchableOpacity>
        </View>

    </View>

)

const styles= StyleSheet.create({
    manuLayout:{
        flex:1,
        flexDirection:'row',
        flexWrap:'wrap',
        margin:8
      },
      menuItem:{
        width:'50%',
        height:'50%',
        padding:16,
      },
      menu:{
        padding:16,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#F7941D',
        borderRadius:12,
        shadowColor: "#000000",
        shadowOpacity: 0.1,
        shadowRadius: 2,
        shadowOffset: {
          height: 1,
          width: 0
        }
      },
      menuTitle:{
        color:'#FFF',
        fontSize:16,
        textAlign:'center'
      },
      menuIcon:{
        margin:5,
        width:50,
        height:50
      }
})
export default Menu