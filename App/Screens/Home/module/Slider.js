import React, {Component} from 'react'
import{
    Text,
    View,
    Image,
    Dimensions
} from 'react-native'

import Swiper from 'react-native-swiper'
import FastImage from 'react-native-fast-image'

const { width } = Dimensions.get('window')
const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width;

const styles={
    container:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
        width:'100%'
    },
    image:{
        justifyContent: 'center',
        alignItems: 'center',
        width: imageWidth
    }
}

const Slider = props => (
    <View style={styles.container}>
        <Image style={{width:imageWidth}} source={props.uri}/>
    </View>
)

export default class extends Component{
    constructor(props){
        super(props)
        this.state={
            imagesSlider:[
                require('../../../Images/1.png')

            ]
        }
    }

    render(){
        return(
            <View>
                <Swiper autoplay style={styles.image}>
                {
                    this.state.imagesSlider.map((item, i) => <Slider
                        uri={item}
                        key={i}
                    />)
                }
                </Swiper>
            </View>
        )
    }
}
