import React, { Component } from 'react'
import { 
    StyleSheet, 
    View, 
    TouchableOpacity, 
    Image, 
    Text,
    Linking, 
    FlatList,
    AsyncStorage
} from 'react-native'

import {urlDev} from '../../../lib/server'

const _onPressButton = () => {
    Alert.alert('You tapped the button!');
    //navigate('Profile', {name: 'Jane'})
    Linking.openURL('https://drive.google.com/file/d/1K5Mu6QOdzGL24xz3wqBRYGJKVD42Sxr1/view').catch(err => console.error('An error occurred', err));
}

const _onPressButton1 = () => {
    //Alert.alert('You tapped the button!');
    //navigate('Profile', {name: 'Jane'})
    Linking.openURL('https://www.kaskus.co.id/').catch(err => console.error('An error occurred', err));
}



class GridView extends Component {
    constructor() {
        super()
        this.state = {
            responseData: [],
        }
        AsyncStorage.getItem('token', (error, result) => {
            if (result) {
                this.setState({
                    token: result
                });
            }

            return fetch(urlDev + '/api/v1/brochure?filter=[{"property":"event_brochures.category","value":"menu","operator":"like"}]&sort=[{"property":"event_brochures.title","direction":"ASC"}]', {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + result
                },
            })
                .then((resp) => {
                    // console.warn(resp)
                    this.setState({
                        isLoading: false
                    })

                    if (resp.status === 200) {
                        return resp.json()
                    }
                    else if (resp.status === 401) {
                        Alert.alert('Message ', 'Access is denied')
                    } else if (resp.status === 503) {
                        Alert.alert('Message ', 'Service Unavailable')
                    }
                })
                .then((json) => {
                    console.warn(json)
                    if (json.success == true) {
                        this.setState({
                            responseData: json.rows
                        }, function () {

                        })
                    }
                })
                .done()
        })

    }

    render() {
        return (
            <View style={styles.container} >
                <FlatList
                    data={this.state.responseData}
                    renderItem={({ item }) => {
                        <TouchableOpacity style={styles.menuItem} onPress={_onPressButton}>
                            <Image style={styles.menuIcon} source={{ uri: item.icon }} />
                            {/* <Text style={styles.menuTitle}>MyCloud</Text> */}
                        </TouchableOpacity>
                    }
                    } keyExtractor={({ id }, index) => id}
                />

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        alignItems: 'flex-start'
    },
    menuLayout: {
        flex: 1, justifyContent: 'space-between'
    },
    baris: {
        flex: 1, flexDirection: 'row', justifyContent: 'space-between'
    },
    menuItem: {
        paddingLeft: 18,
        paddingRight: 18,
        paddingBottom: 10,
        paddingTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    menuIcon: {
        width: 70,
        height: 50,
    },
    menuTitle: {
        fontSize: 10
    }
})

export default GridView
