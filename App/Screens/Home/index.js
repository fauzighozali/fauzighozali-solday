import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    ScrollView,
    StatusBar,
    BackHandler,
    Platform,
    Alert,
    Linking,
    FlatList,
    ListView,
    AsyncStorage,
    Text,
    TouchableOpacity,
    Image,
    Dimensions
} from 'react-native'

import DeviceInfo from 'react-native-device-info';
import { getAppstoreAppVersion } from "react-native-appstore-version-checker";
import { FlatGrid } from 'react-native-super-grid';

import Menu from './module/Menu'
import Slider from './module/Slider'
import { urlDev } from '../../lib/server'
import { GridItem } from '../../Components/ListItem'

class HomeScreen extends Component {

    constructor() {
        super()
        this.state = {
            responseData: [],
        }
    }

    getDataMyDigital = () => {
        AsyncStorage.getItem('token', (error, result) => {
            if (result) {
                this.setState({
                    token: result
                });
            }
            return fetch(urlDev + '/api/v1/brochure?filter=[{"property":"event_brochures.category","value":"menu","operator":"like"}]&sort=[{"property":"event_brochures.title","direction":"ASC"}]', {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + result
                },
            })
                .then((resp) => {
                    console.warn(resp)
                    this.setState({
                        isLoading: false
                    })

                    if (resp.status === 200) {
                        return resp.json().then((json) => {
                            console.warn(json)
                            if (json.success == true) {
                                this.setState({
                                    responseData: json.rows
                                }, function () {

                                })
                            }
                        })
                    }
                    else if (resp.status === 401) {
                        this.getDataMyDigital()
                    } else if (resp.status === 503) {
                        Alert.alert('Message ', 'Service Unavailable')
                    }
                })

                .done()


        })
    }

    componentDidMount() {
        this.getDataMyDigital();
        if (Platform.OS == "android") {
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
            this.props.navigation.addListener('didFocus', this.load);
            this.props.navigation.addListener('didBlur', this.blur);
        }

        const appName = DeviceInfo.getApplicationName(); // "Learnium Mobile"

    }



    load = () => {
          if (Platform.OS == "android" ) {
            console.log("added");
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

                    getAppstoreAppVersion("com.agit.solutionday") //put any apps packageId here
  .then(appVersion => {
    const version = DeviceInfo.getVersion();
    console.log(version);
    console.log("com.agit.solutionday android app version on playstore", appVersion);

    if(version == appVersion)
    {
        console.log("up to date");
        // BackHandler.exitApp()
        // Linking.openURL("market://details?id=com.agit.solutionday");
    }
    else
    {
        //this.updateConfirmation();
    }
  })
  .catch(err => {
    console.log("error occurred", err);
  });




            //this.props.navigation.addListener('willFocus', this.load)
        }
    }

    blur = () => {
        console.log("blur");
        this.removeHandler();
    }


    removeHandler() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    updateConfirmation = () => {
        Alert.alert(
            'Update your app',
            'Please update before using this app', [ {
                text: 'Update',
                onPress: () => {BackHandler.exitApp(); Linking.openURL("market://details?id=com.agit.solutionday"); }
            },], {
                cancelable: false
            }
        )
        return true;
    }



    handleBackButton = () => {
        Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            },], {
                cancelable: false
            }
        )
        return true;
    }



    // getMyDigitalLife() {
    //     return fetch(urlDev + '/api/v1/brochure?filter=[{"property":"event_brochures.category","value":"menu","operator":"like"}]&sort=[{"property":"event_brochures.title","direction":"ASC"}]', {
    //         method: "GET",
    //         headers: {
    //             'Content-Type': 'application/json',
    //             'Authorization': 'Bearer ' + this.state.token
    //         },
    //     })
    //         .then((resp) => {
    //             console.warn(resp)
    //             this.setState({
    //                 isLoading: false
    //             })

    //             if (resp.status === 200) {
    //                 return resp.json()
    //             }
    //             else if (resp.status === 401) {
    //                 Alert.alert('Message ', 'Access is denied')
    //             } else if (resp.status === 503) {
    //                 Alert.alert('Message ', 'Service Unavailable')
    //             }
    //         })
    //         .then((json) => {
    //             // console.warn(json)
    //             if (json.success == true) {
    //                 this.setState({
    //                     responseData: json.rows
    //                 }, function () {

    //                 })
    //             }
    //         })
    //         .done()

    // }



    render() {
        return (

            <View style={styles.container}>
                <StatusBar backgroundColor="#fff" barStyle="dark-content" />
                <ScrollView style={{ flex: 1 }} >
                    <View style={styles.sliderLayout}>
                        <Slider />
                    </View>
                    <Menu
                        timeline={() => {
                            // this.removeHandler()
                            this.props.navigation.navigate('Timeline')

                        }
                        }
                        games={() => {
                            // this.removeHandler()
                            this.props.navigation.navigate('Games')
                        }
                        }
                        track={() => {
                            // this.removeHandler()
                            this.props.navigation.navigate('Track')
                        }
                        }
                        brochure={() => {

                            this.props.navigation.navigate('Brochure')
                        }
                        }
                    />
                    <View style={{
                        height: 1,
                        shadowColor: '#a0a0a0',
                        borderColor: '#ececec',
                        borderBottomWidth: 1,
                        borderLeftWidth: 1,
                        borderRightWidth: 1,
                        borderTopWidth: 1,
                        borderRadius: 5,
                        marginBottom: 10,
                    }}></View>

                    {/* <CircleMenu /> */}

                    <View style={{ marginLeft: 16, justifyContent: 'center', }}>
                        <View style={{ alignItems: 'center' }}>
                            <Image style={{ marginTop: 10, height: 50, width: 120, }} source={require('../../Images/mydigitallife.png')} />
                            <Text numberOfLines={1} style={{ color: '#ececec', justifyContent: 'center', fontSize: 16 }}>
                                ______________________________
                        </Text>
                        </View>
                        <FlatGrid
                            itemDimension={100}
                            items={this.state.responseData}
                            renderItem={({ item }) => (
                                <GridItem
                                    image={{ uri: item.icon }}

                                    onPress={item.url}
                                />
                            )}
                        />
                    </View>

                </ScrollView>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        backgroundColor: '#fff',
    },
    sliderLayout: {
        height: 200,
        alignItems: 'center',
        justifyContent: 'center',
    },
    list: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    menuItem: {
        paddingLeft: 18,
        paddingRight: 18,
        paddingBottom: 10,
        paddingTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    menuIcon: {
        width: 70,
        height: 50,
    },
})
export default HomeScreen
