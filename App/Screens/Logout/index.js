import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, AsyncStorage } from 'react-native'


import { DialogMessage } from '../../Components/DialogInput'

export default class LogoutScreen extends Component {

    state = {
        isShowDialog: true,
    }

    render() {
        if (this.state.isShowDialog === true) {
            this.props.navigation.navigate('Home')
        }
        return (
            <View>
                {/* <TouchableOpacity onPress={this.props.navigation.navigate('Home')}/> */}
                <DialogMessage
                    visible={this.state.isShowDialog}
                    description={'You are exiting this account?'}
                    close={() =>
                        this.setState({
                            isShowDialog: false
                        })
                    }
                    submit={() => {

                        AsyncStorage.clear()
                        AsyncStorage.setItem('token', '')
                        AsyncStorage.setItem('role', '')
                        this.props.navigation.navigate('Login')
                        this.setState({
                            isShowDialog: false
                        })
                    }}
                />
            </View>
        )
    }
}