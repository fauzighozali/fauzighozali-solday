import React, { Component } from "react";
import {
    Dimensions,
    View,
    Text,
    AsyncStorage,
    ActivityIndicator,
    StyleSheet,
    Modal,
    FlatList,
    TouchableOpacity,
    TextInput,
    ScrollView
} from 'react-native'
import RadioForm from 'react-native-radio-form-custom';
import { ButtonOrange } from '../../Components/PlaceButton'
import { urlDev } from "../../lib/server";
import { DialogMessage2 } from '../../Components/DialogInput'

const width_max = Dimensions.get('window').width
const height_max = Dimensions.get('window').height


class QuestionScreen extends Component {
    constructor() {
        super()
        this.state = {
            token: '',
            dataSource: [],
            dataRankQ: [],
            isLoading: true,
            selectQ: '',
            isDialog: false,
            isShowDialog: false,
            message: '',
            idQ:'',
        }
        AsyncStorage.getItem('token', (error, result) => {
            if (result) {
                this.setState({
                    token: result
                });
                return fetch(urlDev + '/api/v1/masterquestion?sort=[{"property":"order_no","direction":"asc"}]', {
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + result
                    },
                })
                    .then((resp) => {
                        // console.warn(resp)
                        this.setState({
                            isLoading: false
                        })

                        if (resp.status === 200) {
                            return resp.json().then((responseJson) => {
                                console.warn(responseJson)
                                this.setState({ dataSource: responseJson.rows })
                            })
                        }
                        else if (resp.status === 401) {
                            Alert.alert('Message ', 'Access is denied')
                        } else if (resp.status === 503) {
                            Alert.alert('Message ', 'Service Unavailable')
                        }
                    })

                    .catch((error) => {
                        console.error(error);
                    });
            }
        })
    }

    _onSelect = (q, id) => {
        console.warn(q+'dan '+id)
        this.setState({
            selectQ: q,
            idQ:id
        })
    }

    handlerSubmit = (s) => {
        this.setState({
            isLoading: true
        })
        if (this.state.selectQ != '') {
            console.warn(this.state.selectQ + ' ' + this.state.idQ)
            return fetch(urlDev + '/api/v1/question', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.state.token
                },
                body: JSON.stringify({
                    "value": this.state.selectQ,
                    "event_question_id": this.state.idQ,
                    "event_topic_id": s.id

                })
            }).then((resp) => {
                // console.warn(resp)
                this.getRankQuestion()
                this.setState({
                    isLoading: false,
                })

                if (resp.status === 200) {
                    return resp.json().then((responseJson) => {
                        console.warn(responseJson)
                        if (responseJson.success === true) {
                            this.setState({
                                isShowDialog: true,
                                message: responseJson.message,
                            })
                        } else {
                            this.setState({
                                isShowDialog: true,
                                message: responseJson.message,
                            })
                        }
                    })
                }
                else if (resp.status === 401) {
                    Alert.alert('Message ', 'Access is denied')
                } else if (resp.status === 503) {
                    Alert.alert('Message ', 'Service Unavailable')
                }
            })
                .catch((error) => {
                    console.error(error);
                });
        } else {
            this.setState({
                isLoading: false
            })
            alert('Pilih salah satu pertanyaan berikut')
        }
    }

    getRankQuestion = () => {
        return fetch(urlDev + '/api/v1/masterquestion?sort=[{"property":"score","direction":"desc"}]', {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.state.token
            },
        })
            .then((resp) => {
                // console.warn(resp)
                this.setState({
                    isLoading: false
                })

                if (resp.status === 200) {
                    return resp.json()
                }
                else if (resp.status === 401) {
                    Alert.alert('Message ', 'Access is denied')
                } else if (resp.status === 503) {
                    Alert.alert('Message ', 'Service Unavailable')
                }
            })
            .then((responseJson) => {
                console.warn(responseJson)
                this.setState({ dataRankQ: responseJson.rows })
            })
            .catch((error) => {
                console.error(error);
            });
    }

  
    render() {
        const { navigation } = this.props;
        const speaker = navigation.getParam('speaker');
        const track = navigation.getParam('track')

        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    <ActivityIndicator />
                </View>
            )
        }

        if (this.state.isDialog) {
            return (
                <View style={styles.container}>
                    <ScrollView style={{ flex: 1 }}>
                        <Text style={{ fontSize: 18, color: '#F7941D', fontStyle: 'italic', marginBottom: 10, textAlign: 'center', fontWeight: 'bold' }}>TOP QUESTIONS</Text>

                        <View style={{
                            borderColor: '#F7941D',
                            borderRadius: 5,
                            borderWidth: 1,
                            shadowOffset: { width: 2, height: 3 },
                            padding: 5,
                        }}>
                            <FlatList
                                data={this.state.dataRankQ}
                                renderItem={({ item }) =>
                                    <View style={{
                                        flexDirection: 'row',
                                        alignContent: 'space-between',
                                        borderColor: '#ececec',
                                        borderRadius: 5,
                                        borderBottomWidth: 0.5,
                                        padding: 10,
                                        marginBottom: 8
                                    }}>
                                        <Text style={{ fontSize: 16, width: '90%' }}>{item.value}</Text>
                                        <Text style={{ fontSize: 16, width: '10%', textAlign: 'right' }}>{item.score}</Text>
                                    </View>
                                } keyExtractor={({ id }, index) => id}
                            />
                        </View>
                    </ScrollView>
                </View>
            )
        }

        let renderQE = [];
        let datas = this.state.dataSource;
        let renderSQ = [];
        let renderIQ = [];
        let j = 0;
        datas.map(data => {
            if (data.value !== "") {
                renderSQ.push(data)
            } else {
                let id= data.id;
                console.warn(id)
                renderIQ.push(
                    <TextInput
                        style={{
                            height: 40,
                            color: '#606060',
                            paddingLeft: 10,
                            paddingRight: 10,
                        }}
                        placeholder="Write your quetion here..."
                        multiline
                        onChangeText={(text) => this._onSelect(text,data.id)} />
                )
            }
            j++;
        })
        // renderQE.push(renderSQ)
        // renderQE.push(renderIQ)




        return (
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.container}>

                    <RadioForm
                        style={{
                            width: width_max,
                            fontSize: 13,
                            marginLeft: 24,
                            marginRight: 16,
                            marginTop: 5,
                            marginBottom: 3,
                        }}
                        dataSource={renderSQ}
                        itemShowKey="value"
                        itemRealKey="id"
                        circleSize={14}
                        formVertical={true}
                        customTextStyle={{ color: '#7C7C7C', fontSize: 14, marginRight: 8 }}
                        customViewStyle={{
                            flex: 1,
                            paddingLeft: 5,
                            paddingRight: 30,
                            paddingTop: 5,
                            paddingBottom: 5,
                            flexDirection: 'row',
                            borderColor: '#909090',
                            alignItems: 'center',
                            borderRadius: 5,
                            borderWidth: 0.5,
                            marginTop: -0.5,
                            marginBottom: 8,
                            marginRight: 16,
                            width: width_max - 20
                        }}
                        onPress={(item) => this._onSelect(item.value, item.id)}
                    />
                    <View style={{
                        borderRadius: 5,
                        borderWidth: 0.5,
                        borderColor: '#909090',
                        alignItems: 'flex-start',
                        marginBottom: 8,
                        marginRight: 10,
                        marginLeft: 10,
                        width: width_max - 20,
                        padding:10

                    }}>
                        <Text style={{ textAlign: 'left' }}>Other Quetion</Text>
                        {renderIQ}
                    </View>
                    <ButtonOrange
                        onPress={
                            () => this.handlerSubmit(speaker)
                        }
                        title='Submit Question'
                    />
                    <DialogMessage2
                        visible={this.state.isShowDialog}
                        description={
                            this.state.message
                        }
                        submit={() => {
                            this.setState({
                                isShowDialog: false,
                                isDialog: false,
                            })
                        }}
                    />
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
        marginLeft: 16,
        marginRight: 16,
        marginBottom: 16,
        marginTop: 2,
        backgroundColor: '#fff',
        alignItems: 'center'
    }
})

export default QuestionScreen