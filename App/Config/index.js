import React from 'react'
import { StyleSheet, View, Image, Text, Alert, Button } from 'react-native'
import { createAppContainer, createStackNavigator, createDrawerNavigator } from 'react-navigation'
import { BurgerButton, MyProfileButton } from '../Components/PlaceButton'

import SplashScreen from "../Screens/AppIntro/index"
import LoginScreen from "../Screens/Login"
import LogoutScreen from "../Screens/Logout"
import HomeScreen from "../Screens/Home"
import ProfileScreen from "../Screens/Profile"
import TrackScreen from "../Screens/Track/Track"
import SpeakersScreen from "../Screens/Track"
import SessionScreen from "../Screens/Session"
import ScannerGamesScreen from '../Screens/Games/ScannerGames'
import GamesScreen from '../Screens/Games'
import TimelineActivity from "../Screens/Timeline/Timeline"
import BrochureScreen from "../Screens/Brochure"
import FeedbackScreen from "../Screens/Feedback/Feedback"
import ScannerScreen from "../Screens/Scanner"
import EOScreen from '../Screens/EventOrganizer'
import QuestionScreen from '../Screens/Question'

const stylesHeader = StyleSheet.create({
  headerTitleStyle: {
  }
})

const DrawerNavigation = createAppContainer(createDrawerNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        title: 'Home'
      }
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: {
        title: 'Profile'
      }
    },
    Timeline1: {
      screen: TimelineActivity,
      navigationOptions: {
        title: 'Agenda',
      }
    },
    Track1: {
      screen: TrackScreen,
      navigationOptions: {
        title: 'Questions and Track'
      }
    },
    Games1: {
      screen: GamesScreen,
      navigationOptions: {
        title: 'Games'
      }
    },
    Brochure1: {
      screen: BrochureScreen,
      navigationOptions: {
        title: 'Presentation Materials'
      }
    },
    Feedback1: {
      screen:(props) =>  <FeedbackScreen {...props} typeFeedback={'umum'} />,
      navigationOptions: {
        title: 'Feedback'
      }
    },
    Logout: {
      screen: LogoutScreen,
      navigationOptions: {
        title: 'Log Out'
      }
    },
  }
))

const EONavigation = createAppContainer(createStackNavigator({
  EventOrganizer: {
    screen: EOScreen,
    navigationOptions: {
      title: 'AGIT SOLUTION DAY',
      headerTintColor: '#F19720',
      headerTitleStyle: {
        width: '90%',
        textAlign: 'center',
      },
      headerStyle: stylesHeader.headerStyle,
    }
  },
  Scanner: {
    screen:ScannerScreen,
    navigationOptions:{
      title:'Scan QR Code',
      headerTintColor: '#F19720',
      headerTitleStyle: {
        width: '90%',
      },
      headerStyle: stylesHeader.headerStyle,
    }
  }
}))

export const AppNavigation = createAppContainer(
  createStackNavigator(
  {

    Splash: {
      screen: SplashScreen,
      navigationOptions: {
        header: null
      }
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        header: null
      }
    },
    EO:{
      screen:EONavigation,
      navigationOptions:{
        header:null
      }
    },
    Drawer: {
      screen: DrawerNavigation,
      navigationOptions: ({ navigation }) => (
        {

        title: 'AGIT SOLUTION DAY',
        headerTintColor: '#F19720',
        headerTitleStyle: {
          width: '90%',
          textAlign: 'center',
        },
        headerStyle: stylesHeader.headerStyle,
        headerLeft: (<BurgerButton />),
        headerRight: ( <MyProfileButton navigation={navigation}  />)
      })
    },
    Timeline: {
      screen: TimelineActivity,
      navigationOptions: {
        title: 'Agenda',
        headerTintColor: '#F19720',
        headerTitleStyle: {
          width: '90%',
        },
        headerStyle: stylesHeader.headerStyle,
      }
    },
    Speaker: {
      screen: SpeakersScreen,
      navigationOptions: {
        title: 'Speaker List',
        headerTintColor: '#F19720',
        headerTitleStyle: {
          width: '90%',
        },
        headerStyle: stylesHeader.headerStyle,
      }
    },
    Session: {
      screen: SessionScreen,
      navigationOptions: {
        title: 'Session',
        headerTintColor: '#F19720',
        headerTitleStyle: {
          width: '90%',
        },
        headerStyle: stylesHeader.headerStyle,
      }
    },
  Question: {
      screen: QuestionScreen,
      navigationOptions: {
        title: 'Select Question',
        headerTintColor: '#F19720',
        headerTitleStyle: {
          width: '90%',
        },
        headerStyle: stylesHeader.headerStyle,
      }
    },
    Feedback: {
      screen: (props) => <FeedbackScreen {...props} typeFeedback={'track'} />,
      navigationOptions: {
        title: 'Feedback',
        headerTintColor: '#F19720',
        headerTitleStyle: {
          width: '90%',
        },
        headerStyle: stylesHeader.headerStyle,
      }
    },
    ScannerGames: {
      screen: ScannerGamesScreen,
      navigationOptions: {
        title: 'Games',
        headerTintColor: '#F19720',
        headerTitleStyle: {
          width: '90%',
        },
        headerStyle: stylesHeader.headerStyle,
      }
    },

    Track: {
      screen: TrackScreen,
      navigationOptions: {
        title: 'Track',
        headerTintColor: '#F19720',
        headerTitleStyle: {
          width: '90%',
        },
        headerStyle: stylesHeader.headerStyle,
      }
    },
    Games: {
      screen: GamesScreen,
      navigationOptions: {
        title: 'Games',
        headerTintColor: '#F19720',
        headerTitleStyle: {
          width: '90%',
        },
        headerStyle: stylesHeader.headerStyle,
      }
    },
    Brochure: {
      screen: BrochureScreen,
      navigationOptions: {
        title: 'Brochure',
        headerTintColor: '#F19720',
        headerTitleStyle: {
          width: '90%',
        },
        headerStyle: stylesHeader.headerStyle,
      }
    }


  },
  {
    initialRouteName: 'Splash',
    navigationOptions: {
      gesturesEnabled: true,
    },
  }
))
