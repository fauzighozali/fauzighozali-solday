import { AsyncStorage } from 'react-native'

export const saveSessionForLocal = sessionToLocal => {
	return dispatch => {
		AsyncStorage.setItem('session', JSON.stringify(sessionToLocal))
	}
}
