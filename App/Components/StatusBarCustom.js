import React, {Component} from 'react'
import {View, StatusBar} from 'react-native'

export const StatusBarCustom = ()=>{
 return(
     <View>
         <StatusBar backgroundColor="#105D9B" barStyle="light-content" />
     </View>
 )   
}