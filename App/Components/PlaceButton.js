import React,{Component} from "react"
import {StyleSheet, TouchableOpacity, Text, Image,Button} from "react-native"
import { withNavigation } from 'react-navigation'
import { compose, withHandlers, withState } from 'recompose'



const enhance = compose(
  withNavigation,
  withHandlers({
    openDrawer: ({ navigation }) => () => navigation.toggleDrawer(),
    goBack: ({ navigation }) => () => navigation.goBack(),
    navigate: ({ navigation }) => to => () => navigation.navigate({ key: to, routeName: to })
  })
);


export const PlaceButton = props =>{
  return (
    <TouchableOpacity style={styles.buttonStyle}
        onPress={props.onPress}>
        <Text style={styles.textStyle}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export const ButtonOrange = props =>{
  return (
    <TouchableOpacity style={styles.button2Style}
        onPress={props.onPress}>
        <Text style={{
          fontSize:18,
          color: '#fff',
          textAlign: 'center',}}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export const MyProfileButton = props =>{
  
  return (
<TouchableOpacity 
style={{ marginRight: 10 }} 
underlayColor={'transparent'} onPress={() => {props.navigation.navigate('Profile',{})}} >
  <Image style={{width:40, height:40}}  
    source={require('../Images/user.png')} />
</TouchableOpacity>
  )
}

export const BackButton = enhance(({ goBack, iconSize = 40 }) => {
  return (
    <TouchableWithoutFeedback underlayColor={'transparent'} onPress={goBack}>
     <Image style={{width:24, height:24}} soure={require('../Images/ic_menu.png')} />
    </TouchableWithoutFeedback>
  )
});


export const BurgerButton = enhance(({ openDrawer, iconSize = 30 }) => {
  return (
      <TouchableOpacity onPress={openDrawer} style={{ marginLeft: 10 }} 
      underlayColor={'transparent'}>
        <Image style={{width:24, height:24}}  
          source={require('../Images/ic_menu.png')} />
      </TouchableOpacity>
  )
})


const styles = StyleSheet.create({
    textStyle: {
      fontSize:18,
      color: '#F7941D',
      textAlign: 'center',
    },
    
    buttonStyle: {
        width:'100%',
        paddingLeft: 10,
        paddingRight:10,
        paddingTop:5,
        paddingBottom:5,
        justifyContent:'center',
        alignItems:"center",
        backgroundColor:'#fff',
        borderRadius:5,
        borderWidth: 1,
        borderColor: '#FFF',
        borderBottomWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3  },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        marginBottom: 10
    },
    button2Style: {
      width:'100%',
      justifyContent:'center',
      alignItems:"center",
      paddingLeft: 10,
      paddingRight:10,
      paddingTop:5,
      paddingBottom:5,
      backgroundColor:'#F7941D',
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#FFF',
      borderBottomWidth: 1,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 5 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 1,
      marginBottom: 10
  }
  });