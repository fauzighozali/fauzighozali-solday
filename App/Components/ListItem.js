import React, { Component } from "react"
import { StyleSheet, View, Text, TouchableOpacity, Image, Linking } from 'react-native'

export const ListItem = props => (
    <TouchableOpacity onPress={() => { props.onShowDialog(props.id) }}>
        <View style={styles.listItem} >
            <View style={{ justifyContent: 'center' }}>
                <Image
                    style={styles.image}
                    source={props.image}
                />
            </View>
            <View style={styles.item}>
                <Text style={{ fontSize: 16, color: '#303030' }}>{props.name}</Text>
                <Text style={{ fontSize: 13 }}>{props.corporate}</Text>
                <Text style={{ fontSize: 13 }}>{props.title}</Text>
            </View>
        </View>
    </TouchableOpacity>
)

export const GridItem = props => (
    <TouchableOpacity onPress={() => {
        Linking.openURL(props.onPress).catch(err => console.error('An error occurred', err));
    }}>
        <Image
            style={{ height: 70, width: 70 }}
            source={props.image}
        />
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    listItem: {
        flexDirection: 'row',
        paddingLeft: 5,
        paddingRight: 16,
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 16,
        marginRight: 16,
        marginBottom: 5
    },
    item: {
        marginLeft: 10,
        marginRight: 25,
        paddingRight: 3,
        alignItems: 'flex-start'
    },
    image: {
        width: 40,
        height: 40,
        borderRadius: 20,
        shadowColor: '#add',
    }
})
