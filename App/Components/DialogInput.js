import React, { Component } from "react"
import { StyleSheet, View, Modal } from "react-native"

import Dialog from 'react-native-dialog'


export const DialogInput = props => (
    <View>
        <Dialog.Container visible={props.visible}>
            <Dialog.Input
                placeholder={props.placeHolder}
                placeholderTextColor='#F7941D'
                style={styles.dialogInput}
                onChangeText={props.updateCode}
                keyboardType='numeric'
            />
            <Dialog.Button label="Cancel" style={{ color: "#F7941D" }} onPress={props.hendleCancel} />
            <Dialog.Button label="Submit" style={{ color: '#F7941D' }} onPress={props.hendleSubmit} />
        </Dialog.Container>
    </View>
)

export const DialogInput2 = props => (
    <View>
        <Dialog.Container visible={props.visible}>
            <Dialog.Input
                placeholder={props.placeHolder}
                placeholderTextColor='#F7941D'
                style={styles.dialogInput}
                onChangeText={props.updateCode}
                secureTextEntry={true}
            />
            <Dialog.Input
                placeholder={props.placeHolder2}
                placeholderTextColor='#F7941D'
                style={styles.dialogInput}
                secureTextEntry={true}
                onChangeText={props.updateCode2}
            />
            <Dialog.Button label="Cancel" style={{ color: "#F7941D" }} onPress={props.hendleCancel} />
            <Dialog.Button label="Submit" style={{ color: '#F7941D' }} onPress={props.hendleSubmit} />
        </Dialog.Container>
    </View>
)

export const DialogMultiInput = props => (
    <View>
        <Dialog.Container visible={props.visible}>
            <Dialog.Input
                placeholder={props.placeHolder}
                placeholderTextColor='#ddd'
                multiline
                style={styles.dialogMultiInput} />
            <Dialog.Button label="Cancel" style={{ color: "#F7941D" }} onPress={props.hendleCancel} />
            <Dialog.Button label="Submit" style={{ color: '#F7941D' }} onPress={props.hendleSubmit} />
        </Dialog.Container>
    </View>
)

export const DialogMessage = props => (
    <View>
        <Dialog.Container visible={props.visible}>
            <Dialog.Description >
                {props.description}
            </Dialog.Description>
            <Dialog.Button label="No" style={{ color: "#F7941D" }} onPress={props.close} />
            <Dialog.Button label="Yes" style={{ color: '#F7941D' }} onPress={props.submit} />
        </Dialog.Container>
    </View>
)

export const DialogMessage2 = props => (
    <View>
        <Dialog.Container contentStyle={{ alignItems: 'center' }} visible={props.visible}>
            <Dialog.Description style={{ alignItems: 'center', textAlign: 'center' }}>
                {props.description}
            </Dialog.Description>
            <Dialog.Button label="Ok" style={{ color: '#F7941D' }} onPress={props.submit} />
        </Dialog.Container>
    </View>
)

const styles = StyleSheet.create({
    dialogInput: {
        padding: 5,
        textAlign: 'center'
    },
    dialogMultiInput: {
        textAlignVertical: 'top',
        padding: 10,
        borderColor: '#F7941D',
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderRadius: 5,
        height: 300,
    }
})
